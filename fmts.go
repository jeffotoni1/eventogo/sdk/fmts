package fmts

import (
	gcat "github.com/jeffotoni/gconcat"
)

func Concat(strs ...interface{}) string {
	return gcat.Concat(strs...)
}

func ConcatStr(strs ...string) string {
	return gcat.ConcatStr(strs...)
}

func ConcatStrInt(strs ...interface{}) string {
	return gcat.ConcatStrInt(strs...)
}
